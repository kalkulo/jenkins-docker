# README #

Docker images for building various projects

### Use with CES ###

Build the image:
* `make ces-docker`

Compile CES MEX wrappers
* In the ces repository, run: `util/docker-compile-all.sh`

Build documentation (pdf):
* In the ces repository, run: `cd doc/sphinx-doc; ./dockermake html`


### Use with SCoRe ###

Build the image:
* `make score-docker`

Compile SCorE:
* In the score repository, run: `docker run -v $PWD:/PWD -u $(id -u):$(id -g) ...`
