all: ces-docker score-docker

%.build: %-builder
	docker build -t kalkulo/$<:18.04 -t kalkulo/$<:latest $<

copy-mkl:
	mkdir -p ces-builder/mkl
	rsync -r --delete --delete-excluded --copy-links \
	    /opt/intel/mkl/include ces-builder/mkl
	rsync -r -v --delete --delete-excluded --copy-links \
	  --include="lib" \
	  --include="intel64" \
	  --include="libmkl*_avx2.so" \
	  --include="libmkl_core.so" \
	  --include="libmkl_gnu_thread.so" \
	  --include="libmkl_intel_lp64.so" \
	  --exclude="*" \
	  /opt/intel/mkl/lib ces-builder/mkl

jenkins-docker: jenkins.build

ces-docker: copy-mkl jenkins.build ces.build
score-docker: jenkins.build score.build

push: all
	docker push kalkulo/jenkins-builder:18.04
	docker push kalkulo/score-builder:latest
	docker push kalkulo/ces-builder:latest

